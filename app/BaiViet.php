<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaiViet extends Model
{

    protected $table = 'baiviet';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function user(){
        return $this->belongsTo('App\User','MaNhanVien');
    }
    public function chude(){
        return $this->belongsTo('App\ChuDe','MaChuDe');
    }
    public function baiviet(){
        return $this->hasMany('App\BaiViet','MaBaiViet');
    }
    public function hinhanh(){
        return $this->hasMany('App\BinhLuan','MaBaiViet');
    }
}
