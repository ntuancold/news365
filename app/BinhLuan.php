<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinhLuan extends Model
{
    protected $table = 'binhluan';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function baiviet(){
        return $this->belongsTo('App\BaiViet','MaBaiViet');      
    }
    public function user(){
        return $this->belongsTo('App\User','MaNhanVien');
    }
}
