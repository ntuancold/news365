<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChuDe extends Model
{
    protected $table = 'chude';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function baiviet(){
        return $this->hasMany('App\BaiViet','MaChuDe');
    }
}
