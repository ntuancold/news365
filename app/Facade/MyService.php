<?php

namespace App\Facade;
use Illuminate\Support\Facades\Facade;

class MyService extends Facade{
    protected static function getFacadeAccessor() {
        return 'MyService_ser'; //'MyURL_ser' alias name for the façade class declare in the class 'NewFacadeServiceProvider'
    }
}
