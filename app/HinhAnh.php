<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HinhAnh extends Model
{
    protected $table = 'hinhanh';
	protected $primaryKey = 'id';
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    public function user(){
        return $this->belongsTo('App\User','MaNhanVien');
    }
    public function baiviet(){
        return $this->belongsTo('App\BaiViet','MaBaiViet');
    }
}
