<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BaiViet;
use App\BinhLuan;
use App\ChuDe;
use App\Http\Resources\BaiViet as BaiVietResource;


class BaiVietAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function load_data(Request $request)
    {

        // $baiviet = BaiViet::where('id','>',request('id'))->limit(5)->get();
        $baiviet = BaiViet::where('id','<',$request->id)->orderBy('created_at','desc')->offset($request->offset)->limit(3)->get();

        return BaiVietResource::collection($baiviet);
    }
    public function danhsach(Request $request)
    {
        $baiviet = BaiViet::all();

        return BaiVietResource::collection($baiviet);
    }
    public function search(Request $request)
    {

        $baiviet = BaiViet::where('TieuDe','like',"%$request->search%")
        ->orWhere('TomTat','like',"%$request->search%")
        ->orWhere('NoiDung','like',"%$request->search%")
        ->limit(5)->orderBy('created_at','desc')->get();

        return BaiVietResource::collection($baiviet);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
