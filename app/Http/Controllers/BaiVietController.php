<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use App\BaiViet;
use App\BinhLuan;
use App\ChuDe;
use App\Facade\MyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
class BaiVietController extends Controller
{
    public function getThemBaiViet(){
        $chude = ChuDe::all();
        return view('baiviet.them',['chude' => $chude]);
    }
    public function postThemBaiViet(Request $request){
        $this->validate($request, [
            'TieuDe' => 'required|unique:baiviet', //unique là không được trùng lại trong bảng(vd: baiviet)
            'NoiDung' => 'required',
            'TomTat' => 'required|max:400',
            'HinhAnh_Bia' => 'required'
        ]);
        // $request->validate([
        //     'TieuDe' => 'required|unique:baiviet', //unique là không được trùng lại trong bảng(vd: baiviet)
        //     'NoiDung' => 'required',
        //     'TomTat' => 'required|max:400',
        //     'HinhAnh_Bia' => 'required'
        // ]);

        if($request->hasFile('HinhAnh_Bia')){
            // Get filename with the extension
            $filenameWithExt1 = $request->file('HinhAnh_Bia')->getClientOriginalName();
            // Get just filename
            $filename1 = pathinfo($filenameWithExt1, PATHINFO_FILENAME);
            //Phần mở rộng của file(Đuôi)
            $extension1 = $request->file('HinhAnh_Bia')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore1= $filename1.'_'.time().'.'.$extension1;
            // Upload Image
            $path1 = $request->file('HinhAnh_Bia')->storeAs('public/cover_images', $fileNameToStore1);
        }
        else
        {
            $fileNameToStore1 = 'noimage.jpg';
        }
        //khởi tạo
        $baiviet = new BaiViet();

        $baiviet->TieuDe = $request->input('TieuDe');
        $baiviet->TomTat = $request->input('TomTat');
        $TieuDe = $request->input('TieuDe');
        $baiviet->NoiDung = $request->input('NoiDung');

        $baiviet->HinhAnh_Bia =  $fileNameToStore1;
        $baiviet->MaChuDe = $request->input('MaChuDe');
        $baiviet->TieuDeKhongDau = MyService::chuanurl($TieuDe);
        $baiviet->MaNhanVien = auth()->user()->id;
        $baiviet->save();
        return redirect('/baiviet/danhsach')->with('success','Tạo bài viết thành công');
    }
    public function getXoaBaiViet(Request $request, $id){
        $baiviet = BaiViet::find($id);
        if (!isset($baiviet)){
            return redirect('/baiviet/danhsach')->with('error', 'No Post Found');
        }

        //Check for correct user
        // if(auth()->user()->id !== $baiviet->MaNhanVien){
        //     return redirect('')->with('error','UnAuthorized Page');
        // }

        if($baiviet->HinhAnh_Bia != 'noimage.jpg'){
            // Delete Image
            Storage::delete('public/cover_images/'.$baiviet->HinhAnh_Bia);
        }
        $baiviet->delete();
        return redirect('/baiviet/danhsach')->with('success','Bài viết được xóa bỏ');
    }
    public function getDanhSachBaiViet(){
        $chude = ChuDe::all();
        $baiviet = BaiViet::orderBy('created_at','desc')->paginate(10);
        return view('baiviet.danhsach',['baiviet' => $baiviet, 'chude' => $chude]);
    }
    public function getNoiDungBaiViet($tieudebaiviet){
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $date->subDay(30);
        $baiviet = BaiViet::where('TieuDeKhongDau',$tieudebaiviet)->first();
        $baivietkhac = BaiViet::where('created_at','>',$date)->where('id','!=',$baiviet->id)->inRandomOrder()->take(5)->get();

        $tenbaiviet = $baiviet->TieuDe;
        $id = $baiviet->id;
        if(auth()->check() == false){

            // thời gian tồn tại session là 10s (testing)


            $baivietKey = 'baiviet_'.$baiviet->id;


            if(Session::has($baivietKey)){
                if(time() - (int)Session::get($baivietKey) > 60){
                    // 60s cho mỗi lượt xem tính theo id user vào id bài viết
                    session()->forget($baivietKey);
                }
            }else{
                $baiviet->increment('LuotXem');
                Session::put($baivietKey,time());
            }
        }
        else
        {
            $id_user = auth()->user()->id;
            // thời gian tồn tại session là 10s (testing)


            $baivietKey = $id_user.'baiviet_'.$baiviet->id;


            if(Session::has($baivietKey)){
                if(time() - (int)Session::get($baivietKey) > 60){
                    // 60s cho mỗi lượt xem tính theo id user vào id bài viết
                    session()->forget($baivietKey);
                }
            }else{
                $baiviet->increment('LuotXem');
                Session::put($baivietKey,time());
            }
        }

        $binhluan  = BinhLuan::where('MaBaiViet','=',$id)
        ->where('KichHoat','1')
        ->orderBy('created_at','desc')
        ->paginate(5);
        return view('baiviet.chitietbaiviet',['baiviet'=>$baiviet,'binhluan' => $binhluan, 'tenbaiviet' => $tenbaiviet , 'baivietkhac' => $baivietkhac ])->with('idbaiviet',$id);
    }



    public function getSuaBaiViet($id){
        $chude = ChuDe::all();
        $baiviet = BaiViet::find($id);

        return view('baiviet.sua',['baiviet' => $baiviet, 'chude' => $chude]);
    }
    public function postSuaBaiViet(Request $request){
        $baiviet = BaiViet::find($request->id);

        $this->validate($request,[
            'TieuDe' => 'required',
            'NoiDung' => 'required',
            'TomTat' => 'required|max:400',
            'HinhAnh_Bia' => 'required'
        ]);

        if($request->hasFile('HinhAnh_Bia')){
            // Get filename with the extension
            $filenameWithExt1 = $request->file('HinhAnh_Bia')->getClientOriginalName();
            // Get just filename
            $filename1 = pathinfo($filenameWithExt1, PATHINFO_FILENAME);
            //Phần mở rộng của file(Đuôi)
            $extension1 = $request->file('HinhAnh_Bia')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore1= $filename1.'_'.time().'.'.$extension1;
            // Upload Image
            $path1 = $request->file('HinhAnh_Bia')->storeAs('public/cover_images', $fileNameToStore1);
        }
        else
        {
            $fileNameToStore1 = 'noimage.jpg';
        }

        $TieuDe = $baiviet->TieuDe = $request->TieuDe;
        $baiviet->TieuDeKhongDau = MyService::chuanurl($TieuDe);
        $baiviet->TomTat = $request->TomTat;
        $baiviet->NoiDung = $request->NoiDung;
        $baiviet->MaChuDe = $request->MaChuDe;
        $baiviet->HinhAnh_Bia = $fileNameToStore1;
        $baiviet->save();

        return \redirect('/baiviet/danhsach');
    }
    public function postBinhLuanBaiViet(Request $request){
        $this->validate($request,[
            'NoiDung' => 'required|max:600'
        ]);
        $tieudekhongdau = $request->input('TieuDeKhongDau');
        if(isset(auth()->user()->name) == false)
        {
            return redirect()->route('login')->with('error','Bạn cần đăng nhập để bình luận');
            // return redirect()->route('login',['thamsoneuco'=>$thamsoneuco])->with('error','Bạn cần đăng nhập để bình luận');
        }
        else{

        $binhluan_them = new BinhLuan();
        $binhluan_them->NoiDung = $request->input('NoiDung');
        $binhluan_them->MaBaiViet = $request->input('MaBaiViet');
        $binhluan_them->MaNhanVien = auth()->user()->name;
        $binhluan_them->save();

        return redirect("/baiviet/tieude/$tieudekhongdau")->with('success','Thêm bình luận thành công');
        }
    }
    public function getDanhSachBinhLuan(){
        $binhluankichhoat = BinhLuan::where('KichHoat','=','1')->orderBy('created_at','desc')->paginate(10);
        $binhluankokichhoat = BinhLuan::where('KichHoat','=','0')->orderBy('created_at','desc')->paginate(10);
        return view('binhluan.danhsach',[
        'binhluankichhoat' => $binhluankichhoat,
        'binhluankokichhoat' => $binhluankokichhoat]);
    }
    public function getBoKichHoatBinhLuan($id){
        $binhluan = BinhLuan::find($id);

        $binhluan->KichHoat = "0";
        $binhluan->save();
        return \redirect()->back()->with('success','Bỏ kích hoạt bình luận thành công');
    }
    public function getKichHoatBinhLuan($id){
        $binhluan = BinhLuan::find($id);

        $binhluan->KichHoat = "1";
        $binhluan->save();
        return \redirect()->back()->with('success','Kích hoạt bình luận thành công');;
    }
    public function getXoaBinhLuan($id){
        $binhluan = BinhLuan::find($id);


        if(Auth::check() == false || auth()->user()->quyenhan != "quanly" && $binhluan->MaNhanVien != auth()->user()->name)
        {
            return \redirect()->back()->with('error','Bạn không có quyền để sử dụng dịch vụ này');
        }
        elseif(auth()->user()->quyenhan == "quanly" || $binhluan->MaNhanVien == auth()->user()->name)
        {
            if($binhluan != null){
                $binhluan->delete();
                return \redirect()->back()->with('success','Bạn xóa bình luận thành công');
            }
            return \redirect()->back()->with('error','Bạn xóa bình luận thất bại');
        }
    }
    public function postSuaBinhLuan(Request $request){
        $this->validate($request,[
            'NoiDung' => 'required|max:600'
        ]);
        $binhluan = BinhLuan::find($request->ID);

        if(auth()->user()->quyenhan == "quanly" || $binhluan->MaNhanVien == auth()->user()->name)
        {
            if($binhluan != null){
                $binhluan->NoiDung = $request->NoiDung;
                $binhluan->save();

                return redirect()->back()->with('success','Đã lưu');
            }
            return \redirect()->back()->with('error','Lưu thất bại');
        }
    }
}
