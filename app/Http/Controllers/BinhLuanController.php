<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\BinhLuan;
use Illuminate\Http\Request;
use App\Http\Resources\BinhLuan as BinhLuanResource;

class BinhLuanController extends Controller
{

    public function BinhLuanADD(Request $request)
    {
        if($request->csrf_token != null)
        {
            $token = $request->csrf_token;
            $user = User::where('remember_token',$token)->first();

            if($user != null)
            {
                $binhluan = new BinhLuan();
                $binhluan->NoiDung = $request->input('noidung');
                $binhluan->MaBaiViet = $request->input('mabaiviet');
                $binhluan->MaNhanVien = $request->input('manhanvien');
                $binhluan->save();
                return response()->json(['add' => 'Add bình luận thành công']);
            }
            else
                return response()->json(['adđ' => 'Add bình luận thất bại']);
        }
    }
    public function Index(Request $request)
    {
        $binhluan = BinhLuan::where('MaBaiViet',$request->id)->where('KichHoat',1)->orderBy('created_at','desc')->paginate(5);
        return BinhLuanResource::collection($binhluan);
    }
    public function BinhLuanDelete(Request $request)
    {
        if($request->csrf_token != null )
        {
        $token = $request->csrf_token;
        $user = User::where('remember_token',$token)->first();
        $binhluan = BinhLuan::find($request->id);
        $manhanvien = $binhluan->MaNhanVien;
        $token = $request->csrf_token;

        $user_name = $user->name;
        $user_quyenhan = $user->quyenhan;

            if($user_name == $manhanvien || $user_quyenhan == 'quanly')
            {
                $binhluan->delete();
                return response()->json(['delete' => 'Delete thành công']);
            }
            else
                return response()->json(['delete' => 'Delete thất bại']);
        }
    }

    public function BinhLuanUpdate(Request $request)
    {
        if($request->csrf_token != null )
        {
        $token = $request->csrf_token;
        $user = User::where('remember_token',$token)->first();
        $binhluan = BinhLuan::find($request->id);
        $manhanvien = $binhluan->MaNhanVien;
        $token = $request->csrf_token;

        $user_name = $user->name;
        $user_quyenhan = $user->quyenhan;

            if($user_name == $manhanvien || $user_quyenhan == 'quanly')
            {
                $binhluan->NoiDung = $request->NoiDung;
                $binhluan->save();
                return response()->json(['update' => 'Update thành công']);
            }
            else
                return response()->json(['update' => 'Update thất bại']);
        }

    }
    public function BinhLuanBokichhoat(Request $request)
    {
        if($request->csrf_token != null )
        {
        $token = $request->csrf_token;
        $user = User::where('remember_token',$token)->first();
        $binhluan = BinhLuan::find($request->id);
        $token = $request->csrf_token;

        $user_quyenhan = $user->quyenhan;
            if($user_quyenhan == 'quanly')
            {
                $binhluan->KichHoat = 0;
                $binhluan->save();
                return response()->json(['active' => 'Active thành công']);
            }
            else
                return response()->json(['active' => 'Actie thất bại']);
        }
    }


    public function hack(){
        return view('quanly.hack');
    }
}
