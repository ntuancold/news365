<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use App\BaiViet;
use App\BinhLuan;
use Illuminate\Http\Request;
use App\ChuDe;
use App\Facade\MyURL;
use App\Http\Resources\BaiViet as BaiVietResource;

class ChuDeController extends Controller
{
    public function getTenChuDe($tenchude){
        $chude = ChuDe::all();
        $chude_search = ChuDe::where('TenChuDeKhongDau',$tenchude)->first();

        $TenChuDe = $chude_search->TenChuDe;
        $MaChuDe = $chude_search->id;

        $baiviet = BaiViet::where('MaChuDe',$MaChuDe)->orderBy('created_at','desc')->get()->toArray();
        if(count($baiviet) > 9)
        {
            $baiviet1 = array_slice($baiviet,0,1);
            $baiviet2 = array_slice($baiviet,1,2);
            $baiviet3 = array_slice($baiviet,3,3);
            $baiviet4 = array_slice($baiviet,6,3);
            $baiviet_last = Arr::last($baiviet4);
            $lastid = $baiviet_last['id'];
            return view('chude.index',[
                'baiviet1' => $baiviet1,
                'baiviet2' => $baiviet2,
                'baiviet3' => $baiviet3,
                'baiviet4' => $baiviet4,

                'chude' => $chude,
                'TenChuDe' => $TenChuDe,
                'MaChuDe'=> $MaChuDe,
                'lastid' => $lastid
                ]);
        }else{

        // $chudecantim = ChuDe::where('TenChuDeKhongDa')->get();
        // $baiviet = ChuDe::where('TenChuDeKhongDau',$tenchude)->first()->baiviet->all();
            $ND = "Chủ đề được câp nhât";
            $ND2 = "Chủ đề đang được cập nhật (Số lượng bài viết thuộc chủ đề phải > 9, Cần tạo thềm bài viết!!!)";
            if(auth()->check() == true && auth()->user()->quyenhan != "quanly"){
                return redirect()->back()->with('error',$ND);
            }else
                return redirect()->back()->with('error',$ND2);
        }
    }
    public function getIndex(){
        $chude = ChuDe::all();

		return view('chude.quanly', ['chude' => $chude]);
    }
    public function getSuaChuDe(Request $request){
        $chude = ChuDe::all();
        $chude_find = ChuDe::find($request['id']);
        return view('chude.sua',['chude'=> $chude , 'chude_find' => $chude_find]);
    }
    public function postSuaChuDe(Request $request){

        $chude = ChuDe::find($request->input('ID'));
        $this->validate($request, [
            'TenChuDe' => 'required'
        ]);
        $TenChuDe = $request->input('TenChuDe');

        $chude->TenChuDe = $TenChuDe;
        $chude->TenChuDeKhongDau = MyURL::chuanurl($TenChuDe);
        $chude->save();
        return redirect('/quanly/chude')->with('success','Đã thay đổi');
    }
    public function getThemChuDe(){
        $chude = ChuDe::all();

        return view('chude.them',['chude' => $chude]);
    }
    public function postThemChuDe(Request $request){
        $this->validate($request, [
            'TenChuDe' => 'required|max:30|unique:chude'
        ]);

        $chude = new ChuDe();
        $TenChuDe = $request->input('TenChuDe');
        $chude->TenChuDe = $TenChuDe;
        $chude->TenChuDeKhongDau = MyURL::chuanurl($TenChuDe);
        $chude->save();
        return redirect('quanly/chude')->with('success','Bài Viết Được Tạo');
    }
    public function getXoaChuDe($id){

        $chude = ChuDe::find($id);
        if(isset($chude) == null){
            return redirect('quanly/chude')->with('error','Chủ đề không tồn tại');
        }
        else{
            $chude->delete();
            return redirect('quanly/chude')->with('success','Bạn xóa chủ đề thành công');
        }
    }
    public function ChuDeBaiVietLoadMore(){
        $baiviet = BaiViet::where('id','<',request('id'))->where('MaChuDe',request('machude'))->orderBy('created_at','desc')->offset(request('offset'))->limit(3)->get();
        return BaiVietResource::collection($baiviet);
    }
}
