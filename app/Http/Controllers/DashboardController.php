<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $hi = 'Xin chào';
        return view('dashboard',['hello' => $hi]); 
    }
}
