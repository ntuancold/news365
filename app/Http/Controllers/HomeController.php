<?php


namespace App\Http\Controllers;
use App\BaiViet;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $baiviet = BaiViet::orderBy('created_at','desc')->take(15)->get()->toArray();

        $baiviet1 = array_slice($baiviet,0,1);
        $baiviet2 = array_slice($baiviet,1,2);
        $baiviet3 = array_slice($baiviet,3,3);
        $baiviet4 = array_slice($baiviet,6,3);
        $baiviet5 = array_slice($baiviet,9,2);
        $baiviet6 = array_slice($baiviet,11,4);

        $baivietcuoi = Arr::last($baiviet6);
        $last_id = $baivietcuoi['id'];
        $dt1 = Carbon::now('Asia/Ho_Chi_Minh');

        $dt1->subDay(60);

        $baiviet_top = BaiViet::where('created_at','>',$dt1)->orderBy('LuotXem','desc')->take(5)->get()->toArray();
        if(isset($baiviet_top) == null){
            $dt2 = Carbon::now('Asia/Ho_Chi_Minh');

            $dt2->subDay(90);
            $baiviet_top = BaiViet::where('created_at','>',$dt2)->orderBy('LuotXem','desc')->take(5)->get()->toArray();
            return $baiviet_top;
        }
        $baiviet_top1 = array_slice($baiviet_top,0,1);
        $baiviet_top2 = array_slice($baiviet_top,1,1);
        $baiviet_top3 = array_slice($baiviet_top,2,3);


       return view('home.index',
       [
        'baiviet1' => $baiviet1,
        'baiviet2' => $baiviet2,
        'baiviet3' => $baiviet3,
        'baiviet4' => $baiviet4,
        'baiviet5' => $baiviet5,
        'baiviet6' => $baiviet6,
        'baiviet_top1' => $baiviet_top1,
        'baiviet_top2' => $baiviet_top2,
        'baiviet_top3' => $baiviet_top3,
        'last_id' => $last_id
       ]);
    }





    public function getLogout(){
        Auth::logout();
        return redirect('');
    }
    public function getQuanly(){
        return view('quanly.index');
    }
}

