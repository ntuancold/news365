<?php

namespace App\Http\Controllers;

use App\BaiViet;
use Illuminate\Http\Request;

class LoadMoreDataController extends Controller
{
    function index()
    {
     return view('home.index');
    }
    function load_data(Request $request)
    {
     if($request->ajax())
     {
      if($request->id > 0)
      {
       $data = BaiViet::
            where('id', '<', $request->id)
          ->orderBy('id', 'DESC')
          ->limit(5)
          ->get();
      }
      else
      {
       $data = BaiViet::
            orderBy('id', 'DESC')
          ->limit(5)
          ->get();
      }
      $output = '';
      $last_id = '';

      if(!$data->isEmpty())
      {
       foreach($data as $value)
       {
        $output .= '
        <div class="container">
            <div class="row ml-3">
                <div class="card mb-3 mt-4" style="width: 1105px;">
                        <div class="row no-gutters">
                            <div class="col-2">
                                <a href="/baiviet/tieude/'.$value['TieuDeKhongDau'].'"><img src="/storage/cover_images/'.$value['HinhAnh_Bia'].'" class="card-img" style="height: 200px;width: 200px;"alt="..."></a>

                            </div>
                        <div class="col-8" style="padding-left:55px;">
                            <a href="/baiviet/tieude/'.$value['TieuDeKhongDau'].'">
                                <div class="card-body">
                                <h5 class="card-title">'.$value['TieuDe'].'</h5>
                                <p class="card-text">'.$value['TomTat'].'</p>
                                <p class="card-text"><small class="text-muted">Đăng vào lúc: '.$value['created_at'].'</small></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ';
        $last_id = $value->id;
       }
       $output .= '
       <div class="container">
       <div id="load_more">
        <button type="button" name="load_more_button" class="btn btn-success form-control" data-id="'.$last_id.'" id="load_more_button">Load More</button>
       </div>
       </div>
       ';
      }
      else
      {
       $output .= '
       <div class="container">
       <div id="load_more">
        <button type="button" name="load_more_button" class="btn btn-info form-control">No Data Found</button>
       </div>
       </div>
       ';
      }
      echo $output;
     }
    }


    //

}
