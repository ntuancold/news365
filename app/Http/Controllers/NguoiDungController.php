<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class NguoiDungController extends Controller
{
    public function getDanhSachNguoiDung(){
        $nguoidung = User::all();

        return view('nguoidung.danhsach',['nguoidung'=>$nguoidung]);
    }
    public function getSuaNguoiDung(Request $request,$manguoidung){
        $nguoidung = User::find($manguoidung);

        return view('nguoidung.sua',['nguoidung' => $nguoidung]);
    }
    public function postSuaNguoiDung(Request $request){
        $nguoidung = User::find($request->input('ID'));
        $this->validate($request,[
            'email' => 'required',
            'name' => 'required'
        ]);
        if($nguoidung != null){

            $TenNguoiDung = $request->input('name');
            $Email = $request->input('email');
            $QuyenHan = $request->input('quyenhan');
            $Khoa = $request->input('khoa');

            $nguoidung->name = $TenNguoiDung;
            $nguoidung->email = $Email;
            $nguoidung->khoa = $Khoa;
            $nguoidung->quyenhan = $QuyenHan;

            $nguoidung->save();

            return \redirect('/quanly/nguoidung/danhsach')->with('success','Sửa người dùng thành công');
        }
        else
        return \redirect('/quanly/nguoidung/danhsach')->with('error','Không tìm thấy người dùng');
    }
    public function getKichHoatNguoiDung($id){
        $nguoidungkh = User::find($id);

        if($nguoidungkh != null)
        {
            $nguoidungkh->khoa = 1;
            $nguoidungkh->save();
            return \redirect('/quanly/nguoidung/danhsach')->with('success',"Kích hoạt $nguoidungkh->name thành công");
        }
        else{

            $nguoidungkh2 = User::where('name',$id)->first();
            if($nguoidungkh2 != null){
                $nguoidungkh2->khoa = 1;
                $nguoidungkh2->save();
                return \redirect()->back()->with('success',"Kích hoạt $nguoidungkh2->name thành công");
            }
            else{
                return \redirect()->back()->with('error',"Kích hoạt thất bại");
            }
        }
        return \redirect('/quanly/nguoidung/danhsach')->with('error',"Kích hoạt thất bại");
    }
    public function getKhoaNguoiDung($id){
        $nguoidungkhoa = User::find($id);

        if($nguoidungkhoa != null)
        {
            $nguoidungkhoa->khoa = 0;
            $nguoidungkhoa->save();
            return \redirect('/quanly/nguoidung/danhsach')->with('success',"Khóa $nguoidungkhoa->name thành công");
        }
        else{

            $nguoidungkhoa2 = User::where('name',$id)->first();
            if($nguoidungkhoa2 != null){
                $nguoidungkhoa2->khoa = 0;
                $nguoidungkhoa2->save();
                return \redirect()->back()->with('success',"Khóa $nguoidungkhoa2->name thành công");
            }
            else{
                return \redirect()->back()->with('error',"Khóa thất bại");
            }
        }
        return \redirect('/quanly/nguoidung/danhsach')->with('error',"Khóa thất bại");
    }
    public function getDoiMatKhau(Request $request){
        if(auth()->check() == true){
            $id = auth()->user()->id;
            $nguoidung = User::find($id);
        return view('nguoidung.doimatkhau',['nguoidung' => $nguoidung]);
        }
        else{
            return view('nguoidung.doimatkhau');
        }
    }
    public function postDoiMatKhau(Request $request){

        $tennguoidung = $request->name;
        $email = $request->email;
        $matkhau = $request->password;
        $matkhaunhaplai = $request->repassword;

        $nguoidung = User::where('email',$email)->first();
        if($matkhau == $matkhaunhaplai){
            $nguoidung->name = $tennguoidung;
            $nguoidung->email = $email;
            $nguoidung->password = Hash::make($matkhau);
            $nguoidung->save();

            return \redirect('/nguoidung/doimatkhau')->with('success','Thay đổi thông tin thành công');
        }
        elseif($matkhau != $matkhaunhaplai){
            return \redirect('/nguoidung/doimatkhau')->with('error','Mật khẩu nhập lại không trùng khớp');
        }
    }
    public function getXoaNguoiDung($id){
        $nguoidung = User::find($id);


        if(auth()->check() == true && auth()->user()->quyenhan == "quanly"){
            if($nguoidung != null)
            {
                $nguoidung->delete();
                return \redirect()->back()->with('success','Bạn xóa người dùng thành công');
            }
            return \redirect()->back()->with('error','Không tìm thấy người dùng!!!');
        }else
            return \redirect()->back()->with('error','Bạn không không đủ quyền để sử dụng dịch vụ này!!!');

    }
}
