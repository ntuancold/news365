<?php

namespace App\Http\Middleware;

use Closure;

class QuanLy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->quyenhan != "quanly"){
            return \redirect('')->with('error','Bạn không đủ quyền để sử dụng dịch vụ này');
        }
        return $next($request);
    }
}
