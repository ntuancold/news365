<?php

namespace App\Http\Middleware;

use Closure;

class checkKichHoat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->khoa == 0){
            auth()->logout();
            return \redirect()->route('login')->with('error','Tài khoản bạn bị tạm khóa');
        }
        return $next($request);
    }
}
