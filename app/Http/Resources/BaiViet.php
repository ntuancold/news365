<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BaiViet extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);


        return [
            'id' => $this->id,
            'TieuDe' => $this->TieuDe,
            'TomTat' => $this->TomTat,
            'created_at' => $this->created_at,
            'TieuDeKhongDau' => $this->TieuDeKhongDau,
            'HinhAnh_Bia' => $this->HinhAnh_Bia
        ];
    }
}
