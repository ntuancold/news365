<?php

namespace App\Http\Resources;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BinhLuan extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'NoiDung' => $this->NoiDung,
            'MaNhanVien'=> $this->MaNhanVien,
            'created_at' => $this->created_at
        ];
    }
}
