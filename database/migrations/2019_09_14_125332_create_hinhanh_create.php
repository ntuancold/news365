<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHinhanhCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hinhanh', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('MaChuDe');
            $table->integer('MaNhanVien');
            $table->string('MoTa');
            $table->string('ThuMuc');
            $table->integer('LuotXem')->default(1);
            $table->integer('KichHoat')->default(1);
            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hinhanh');
    }
}
