@extends('layouts.admin')

@section('content')
<div class="container mt-5">
   <div class="row">
       <div class="col"></div>
       <div class="col">
            <div class="card" style="width: 500px;">
                    <div class="card-header" style="background-color: #2F4F4F; color:white;">Quên mật khẩu</div>
                    <div class="card-body">
                            <form action="/forgot_password" method="post">
                                @csrf
                                <div class="form-group">
                                        <label for="E-Mail">E-mail</label>
                                        <input type="email" class="form-control" id="E-Mail" name="email">
                                        <button type="submit" class="btn btn-primary mt-3">Gửi E-Mail</button>
                                </div>
                            </form>
                    </div>
               </div>
       </div>
   </div>
</div>
@endsection
