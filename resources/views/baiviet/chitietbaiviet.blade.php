@extends('layouts.admin')

@section('title',"$tenbaiviet")
@section('content')
<div class="container mt-4" id="chitietbaiviet">
        @php use App\User; @endphp
    <hr>
    <div class="text-center mt-5 mb-5"><img src="/storage/cover_images/{{$baiviet['HinhAnh_Bia']}}" alt="" style="width: 500px; height:auto;"></div>
    <hr>
    <h1 class="text-center mt-5">{{$baiviet['TieuDe']}}</h1>
    <p class="text-center mt-3"><small>Tác giả: -- <span style="font-weight: bold;">{{User::find($baiviet['MaNhanVien'])->name}}</span> -- </small></p>
    <p class="text-center mt-3"><small>Lượt xem: <span style="font-weight: bold;">{{$baiviet['LuotXem']}}</span></small></p>
    <br>

    <div class="row chitietbaiviet">
        <div class="col" id="noidung">
            {!!$baiviet['NoiDung']!!}
        </div>
    </div>
    <div class="row mt-2 ml-0">
    <small>Được đăng bởi: <span style="font-weight: bold;">{{User::find($baiviet['MaNhanVien'])->name}}</span> vào lúc: <span style="font-weight: bold;">{{$baiviet['created_at']->format('m:h:s D/M/Y ')}}</span></small>
    </div>

    <div class="text-center mt-4">
                @if(Auth::check() == true && auth()->user()->quyenhan == "quanly")
                    <a href="/quanly/baiviet/sua/{{$baiviet['id']}}" class="btn" id="button-xemthem"><i class="fas fa-tools"></i> Sửa</a>
                    <a href="/quanly/baiviet/xoa/{{$baiviet['id']}}" class="btn ml-2" id="button-quaylai" onclick='return confirm("Bạn có chắc xóa bài viết này?")' class="btn btn-danger"><i class="fas fa-trash"></i> Xóa</a>
                @endif
    </div>
    <div class="container padding_top_50" id="app">
        <binhluan-wrapper
        :mabaiviet="{{$idbaiviet}}"
        @if(Auth::check())
        :manhanvien="{{json_encode(auth()->user()->name)}}"
        :quyenhan="{{json_encode(auth()->user()->quyenhan)}}"
        @endif
        @if(Auth::check())
            :dangnhap="true"
            :csrf_token="{{ json_encode(auth()->user()->remember_token) }}"
        @else
            :dangnhap="false"
        @endif
        ></binhluan-wrapper>
    </div>
    <hr>
    <h4 class="ml-2">Các bài viết khác</h4>
    @foreach($baivietkhac as $value)
        <div class="mr-auto mt-2">
                <div class="ml-4">
                        <div class="row">
                                <div class="card mb-3 mt-2" style="width: 800px;">
                                        <div class="row no-gutters">
                                          <div class="col-md-4">
                                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                                    <img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="height: 200px;width: 200px;"alt="...">
                                                </a>
                                          </div>
                                          <div class="col-md-8">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                            <div class="card-body">
                                              <h5 class="card-title">{{Str::limit($value['TieuDe'],145)}}</h5>
                                              <p class="card-text">{{Str::limit($value['TomTat'],150)}}</p>
                                              <p class="card-text card-bottom"><small class="text-muted">Đăng vào lúc: {{$value['created_at']}}</small></p>
                                            </div>
                                            </a>
                                          </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
    @endforeach
</div>
<script src="{{asset('js/app.js')}}"></script>
@endsection
