@extends('layouts.admin')

@section('title','Danh sách bài viết')

@section('content')
    <div class="container-fluid ml-4 mr-4 mt-4">
            <div class="row">
                    <div class="col">
                            <h4>Danh sách các bài viết</h4>
                            @foreach($baiviet as $value)
                            <div class="row">
                                    <div class="card mb-3 mt-3" style="width: 600px">
                                            <div class="row no-gutters">
                                                <div class="col-2">
                                                    <a href="/baiviet/tieude/{{$value->TieuDeKhongDau}}"><img src="/storage/cover_images/{{$value->HinhAnh_Bia}}" class="card-img" style="height: 200px;width: 200px;" alt="..."></a>
                                                </div>
                                                <div class="col-8" style="padding-left: 100px;">
                                                <a href="/baiviet/tieude/{{$value->TieuDeKhongDau}}">
                                                    <div class="card-body">
                                                    <h5 class="card-title">{{Str::limit($value['TieuDe'],100)}}</h5>
                                                    <p class="card-text">{{Str::limit($value->TomTat,60)}}</p>
                                                    <p class="card-text card-bottom"><small class="text-muted">Đăng vào lúc: {{$value->created_at}}</small></p>
                                                    @if(Auth::check() == true && auth()->user()->quyenhan == "quanly")

                                                    @endif
                                                    </div>
                                                </a>
                                                </div>
                                            </div>
                                    </div>
                                    <div style="padding-left: 200px;">
                                            @if(Auth::check() == true && auth()->user()->quyenhan == "quanly")
                                                <a href="/quanly/baiviet/sua/{{$value['id']}}" class="btn" id="button-xemthem"><i class="fas fa-tools"></i> Sửa</a>
                                                <a href="/quanly/baiviet/xoa/{{$value['id']}}" class="btn ml-2" id="button-quaylai" onclick='return confirm("Bạn có chắc xóa bài viết này?")' class="btn btn-danger"><i class="fas fa-trash"></i> Xóa</a>
                                            @endif
                                    </div>
                            </div>
                            @endforeach


                    </div>
                    <div class="col">
                        <div id="app">
                            <search-component
                            @if(Auth::check())
                            :quyenhan="{{json_encode(auth()->user()->quyenhan)}}"
                             @else
                            :quyenhan="reader"
                             @endif>

                            </search-component>
                        </div>
                    </div>
            </div>
            <hr>
            <div class="mr-auto" style="padding-left: 360px">
                    {{$baiviet->links()}}
            </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
