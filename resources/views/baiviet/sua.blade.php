@extends('layouts.admin')

@section('content')

<div class="container mt-4" >
    <form action="{{url('/quanly/baiviet/sua')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="form-group">
            <h3>Sửa bài viết</h3>
            <select class="form-control" id="TenChuDe" name="MaChuDe">
                @foreach ($chude as $value)
                    <option value="{{$value['id']}}"><strong>{{$value['TenChuDe']}}</strong></option>
                @endforeach
            </select>
        </div>
        <input type="hidden" name="id" value="{{$baiviet['id']}}">
        <div class="input-group mb-4 mt-4">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Tiêu đề</span>
            </div>
        <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="TieuDe" value="{{$baiviet['TieuDe']}}">
        </div>
        <div class="input-group mb-4 mt-4">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Tóm Tắt</span>
                </div>
            <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="TomTat" value="{{$baiviet['TomTat']}}">
            </div>
        <textarea class="form-control" id="editor1" placeholder="Nội Dung Bài Viết" name="NoiDung">{!!$baiviet['NoiDung']!!}</textarea>
        <div class="form-group mt-4">
            <label for="HinhAnh_Bia">Hình Ảnh Bìa</label>
            <input type="file" class="form-control-file" id="HinhAnh_Bia" name="HinhAnh_Bia">
        </div>
        <button class="btn float-right" type="submit" id="button-xemthem" onclick='return confirm("Bạn có sửa bài viết không?")'><i class="fa fa-cog" aria-hidden="true"></i> Sửa bài viết</button>
    </form>


</div>
<br>

<script>
    CKEDITOR.replace( 'editor1', {
    language: 'vn',
    });


    CKEDITOR.editorConfig = function( config )
    {
    // misc options
    config.height = '700px';
    };
</script>
@endsection

