@extends('layouts.admin')

@section('content')

<div class="container mt-4" >
    <form action="{{url('/quanly/baiviet/them')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="form-group">
            <h3>Thêm bài viết</h3>
            <select class="form-control" id="TenChuDe" name="MaChuDe">
                @foreach ($chude as $value)
                    <option value="{{$value['id']}}">Chủ Đề -- <strong>{{$value['TenChuDe']}}</strong></option>
                @endforeach
            </select>
        </div>
        <div class="input-group mb-4 mt-4">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Tiêu đề</span>
            </div>
            <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="TieuDe">
        </div>
        <div class="input-group mb-4 mt-4">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Tóm tắt</span>
                </div>
                <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="TomTat">
        </div>
    <textarea class="form-control" id="editor1" placeholder="Nội Dung Bài Viết" name="NoiDung"></textarea>
        <div class="form-group mt-4">
            <label for="HinhAnh_Bia">Hình Ảnh Bìa</label>
            <input type="file" class="form-control-file" id="HinhAnh_Bia"p1 name="HinhAnh_Bia">
        </div>
        <button class="btn float-right" id="button-xemthem" type="submit" onclick='return confirm("Bạn có thêm bài viết không?")'><i class="fa fa-plus" aria-hidden="true"></i> Thêm bài viết</button>
    </form>
</div>
<br>

<script>
    CKEDITOR.replace( 'editor1', {
    language: 'vn',
    });
</script>
@endsection
