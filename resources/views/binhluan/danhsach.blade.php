@extends('layouts.admin')

@section('title','Danh sách các bình luận')


@section('content')
    @php
        use App\Facade\MyService;
    @endphp
<div class="container">

        <div class="row">
            <div class="col">
                    <h4 class="mt-5">Bình luận cho phép</h4>
                    @foreach ($binhluankichhoat as $value)

                    <div class="card mt-4" style="height: 300px;width: 400px;">
                        <div class="card-header">
                            {{$value['MaNhanVien']}}
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <p style="font-size: 12pt;">{{MyService::shortcutString($value['NoiDung'],290)}}</p>
                            <footer class="blockquote-footer">{{$value['created_at']}} --
                            @if(Auth::check() == true)
                            @if(Auth::user()->quyenhan == "quanly")
                            <a class="btn btn-warning" href="/quanly/binhluan/bokichhoat/{{$value['id']}}" onclick='return confirm("Bạn có muốn bỏ kích hoạt bình luân này?")'><i class="fas fa-ban"></i> Bỏ kích hoạt</a>
                            @endif

                            @if(Auth::user()->quyenhan == "nguoidung")
                                <a class="btn btn-warning float-right" href="/baiviet/binhluan/baocao/{{$value['id']}}" onclick='return confirm("Bạn có muốn bỏ kích hoạt bình luân này?")'><i class="fas fa-flag"></i> Báo cáo</a>
                            @endif
                            @endif
                            </footer>
                            </blockquote>
                         </div>
                    </div>


                    @endforeach
                    <div class="mt-4">
                        {{$binhluankichhoat->links()}}
                    </div>
            </div>
            <div class="col">
                    <h4 class="mt-5">Bình luận không cho phép</h4>
                    @foreach ($binhluankokichhoat as $value)

                    <div class="card mt-4" style="height: 300px; width: 400px;">
                        <div class="card-header">
                            {{$value['MaNhanVien']}}
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <p style="font-size: 12pt;">{{$value['NoiDung']}}</p>
                            <footer class="blockquote-footer">{{$value['created_at']}} --
                            @if(Auth::check() == true)
                            @if(Auth::user()->quyenhan == "quanly")
                            <a class="btn btn-success" href="/quanly/binhluan/kichhoat/{{$value['id']}}" onclick='return confirm("Bạn có muốn kích hoạt bình luân này?")'><i class="fas fa-check"></i> Kích hoạt</a>
                            @endif
                            @endif
                            </footer>
                            </blockquote>
                         </div>
                    </div>


                    @endforeach
                    <div class="mt-4">
                        {{$binhluankokichhoat->links()}}
                    </div>
            </div>
        </div>
</div>
@endsection
