@extends('layouts.admin')
@section('title',"$TenChuDe")
@section('content')
    @php
        use App\Facade\MyService;
    @endphp


    <div class="container">
            <div class="row mt-4 ">
                <div class="col">
                        {{-- 1 bài viết --}}
                        @if(isset($baiviet1) != null)
                        @foreach($baiviet1 as $value)
                            <div class="card mb-3" style="width: 720px; height:804px;">
                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" style="max-height:500px;" class="card-img-top" alt="..."></a>
                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                    <div class="card-body">
                                            <h5 class="card-title" id="main-card-title">{{Str::limit($value['TieuDe'],100)}}</h5>
                                            <p class="card-text" id="main-card-tomtat">{{Str::limit($value['TomTat'],350)}}</p>
                                            <p class="card-text card-bottom"><small class="text-muted">Đăng vào lúc: {{$value['created_at']}}</small></p>
                                    </div>
                                </a>
                        </div>
                        @endforeach
                        @endif
                </div>
                <div class="col">
                        {{-- 2 bài viết --}}
                        @if(isset($baiviet2) != null)
                        @foreach($baiviet2 as $value)
                        <div class="row mb-1">
                                <div class="card ml-4" style="width: 340px; height: 400px;">
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img-top" style="width: 338px; height: 248px;" alt="..."></a>
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                            <div class="card-body">
                                                <h5 class="card-title">{{MyService::shortcutString($value['TieuDe'],150)}}</h5>
                                            </div>
                                        </a>
                                </div>
                        </div>
                        @endforeach
                        @endif
                </div>
            </div>
    </div>

    <div class="container">
            {{-- 3 bài viết --}}
            <div class="row mt-4">
                    @if(isset($baiviet3) != null)
                    @foreach($baiviet3 as $value)
                        <div class="col">
                            <div class="card mb-3" style="width: 340px;height: 175px;">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="width: 175px;height: 175px;"alt="..."></a>
                                        </div>
                                        <div class="col-md-8">
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                        <div class="card-body">
                                            <p class="card-title ml-5" style="font-size: 13pt">{{MyService::shortcutString($value['TieuDe'],100)}}</p>
                                        </div>
                                        </a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    @endforeach
                    @endif
            </div>
            {{-- 3 bài viết --}}
            <div class="row mt-4">
                @if(isset($baiviet4) != null)
                @foreach($baiviet4 as $value)
                <div class="col">
                        <div class="card" style="width: 340px;">
                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img-top" style="width: 338px; height: 248px;" alt="..."></a>
                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                    <div class="card-body">
                                        <h5 class="card-title">{{MyService::shortcutString($value['TieuDe'],120)}}</h5>
                                    </div>
                                </a>
                        </div>
                </div>
                @endforeach
                @endif
            </div>
    </div>

    <div id="app" class="container">
        <chude-component v-bind:machude="{{$MaChuDe}}" :lastid="{{json_encode($lastid)}}"></chude-component>
    </div>
    <script>
            setTimeout(function()
            {
            var max = 200;
            var tot, str;
            $('.text').each(function() {
                str = String($(this).html());
                tot = str.length;
                str = (tot <= max)
                    ? str
                : str.substring(0,(max + 1))+"...";
                $(this).html(str);
            });
            },500); // Delayed for example only.
        </script>
        <script src="{{ asset('js/app.js') }}"></script>
@endsection
