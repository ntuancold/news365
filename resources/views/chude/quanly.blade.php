@extends('layouts.admin')

@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col">
                <div class="card" style="width:600px;">
                    <div class="card-header"><a href="{{ url('') }}">Trang Chủ</a></div>
                    <div class="card-body">
                        <p><a href="/quanly/chude/them" class="btn btn-primary"><i class="fa fa-plus"></i>Thêm</a></p>
                        <table class="table table-dark table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" width="8%">#</th>
                                    <th scope="col" width="76%">Tên Chủ Đề</th>
                                    <th scope="col" width="8%">Sửa</th>
                                    <th scope="col" width="8%">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count = 1; @endphp
                                @foreach($chude as $value)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $value->TenChuDe }}</td>
                                        <td class="text-center"><a href="/quanly/chude/{{$value->id}}/sua" class="btn btn-outline-warning"><i class="fas fa-wrench"></i>Sửa</a></td>
                                        <td class="text-center"><a href="/quanly/chude/{{$value->id}}/xoa" class="btn btn-outline-danger" onclick='return confirm("Bạn có muốn xóa chủ đề {{$value->TenChuDe}} không?")'><i class="fas fa-trash-alt"></i>Xóa</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



