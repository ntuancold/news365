@extends('layouts.admin')

@section('content')
    <div class="container mt-4" >
            <form action="{{url('/quanly/chude/sua')}}" method="post">
                {{csrf_field()}}
                <label for="TenChuDe">Tên Chủ Đề</label>
                <input type="hidden" value="{{$chude_find['id']}}" name="ID">
                <input type="text" value="{{$chude_find['TenChuDe']}}" name="TenChuDe">
                <button class="btn btn-primary" type="submit" onclick='return confirm("Bạn có muốn sửa chủ đề {{$chude_find->TenChuDe}} không?")'>Sửa chủ đề</button>
            </form>
    </div>
@endsection
