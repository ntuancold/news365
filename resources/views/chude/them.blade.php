@extends('layouts.admin')

@section('content')
<div class="container">
     <form action="{{url('/quanly/chude/them')}}" method="post">
    {{ csrf_field() }}
            <div class="form-group">
                <label for="TenChuDe">Tên chủ đề bài viết <span class="text-danger font-weight-bold">*</span></label>
                <input type="text" class="form-control{{ $errors->has('TenChuDe') ? ' is-invalid' : '' }}" id="TenChuDe" name="TenChuDe" value="{{ old('TenChuDe') }}" placeholder="" required />
            </div>
        <button type="submit" class="btn" id="button-xemthem">Tạo</button>
    </form>
</div>


@endsection
