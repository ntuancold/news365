@if(count($errors)>0)
    @foreach($errors->all() as $error)
    <div class="container">
        <div class="alert alert-danger mt-4">
            {{$error}}
        </div>
    </div>
	@endforeach
@endif

@if(session('success'))
    <div class="container">
            <div class="alert alert-success mt-4">
                    {{session('success')}}
            </div>
    </div>
@endif

@if(session('error'))
    <div class="container">
        <div class="alert alert-danger mt-4">
                {{session('error')}}
        </div>
    </div>
@endif

