{{-- // thời tiết --}}
<div class="container mt-5">
        <a class="weatherwidget-io" href="https://forecast7.com/en/14d06108d28/vietnam/" data-label_1="NEWS 365" data-label_2="Việt Nam" data-theme="original" data-basecolor="#2F4F4F" >VIETNAM VIET NAM</a>
        <script>
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
        </script>
</div>
{{-- footer --}}
<div class="mt-5 pt-5 pb-5 footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 col-xs-12 about-company">
              <a href=""><h3>NEWS 365</h3></a>
              <p class="pr-5 text-white-50"></p>
              <p><a href="https://www.facebook.com/nguyenbinh2301"><i class="fab fa-facebook-f"></i></a><a href="https://www.instagram.com/_ng_binh/"><i class="fab fa-instagram ml-2"></i></a></p>
            </div>
            <div class="col-lg-3 col-xs-12 links">
              <h4 class="mt-lg-0 mt-sm-3">Người phát triển</h4>
                <ul class="m-0 p-0">
                  <li><a href="#">Nguyễn Trung Bình</a></li>
                  <li><a href="#">Phạm Tấn Đạt</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-xs-12 location">
              <h4 class="mt-lg-0 mt-sm-4">Địa chỉ</h4>
              <p>5/5 Hà Hoàng Hổ, Phường Mỹ Xuyên, Thành phố Long Xuyên, An Giang</p>
              <p class="mb-0"><i class="fa fa-phone mr-3"></i>(+84) 837-994-780</p>
              <p><i class="fa fa-envelope-o mr-3"></i>nguyenbinh23011998@gmail.com</p>
            </div>
          </div>
          <div class="row mt-5">
            <div class="col copyright">
              <p class=""><small class="text-white-50">© Copyright by Nguyễn Bình.</small></p>
            </div>
          </div>
        </div>
</div>
