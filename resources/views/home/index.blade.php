@extends('layouts.admin')
@section('title','NEWS 365')
@section('content')
    @php
        use App\Facade\MyService;
    @endphp
        <div class="container">
                <h1 class="mt-4" style="color: black;">Newest</h1>
                <div class="row mt-2 ">
                    @foreach($baiviet1 as $value)
                    <div class="col">
                             <div class="card mb-3" style="width: 720px; height:804px;">
                                    <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img-top" style="max-height: 500px;"alt="..."></a>
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                            <div class="card-body">
                                                    <h5 class="card-title" id="main-card-title">{!!Str::limit($value['TieuDe'],100)!!}</h5>
                                                    <p class="card-text" id="main-card-tomtat">{{Str::limit($value['TomTat'],350)}}</p>
                                                    <p class="card-text card-bottom"><small class="text-muted">Đăng vào lúc: {{$value['created_at']}}</small></p>
                                            </div>
                                        </a>

                            </div>
                    </div>
                    @endforeach
                    <div class="col">
                            @foreach($baiviet2 as $value)
                            <div class="row mb-1">
                                    <div class="card ml-4" style="width: 340px; height: 400px;">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img-top" style="width: 340px; height: 248px;" alt="..."></a>
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{Str::limit($value['TieuDe'],148)}}</h5>
                                                    </div>
                                                </a>
                                            </a>
                                    </div>
                            </div>
                            @endforeach
                    </div>
                </div>
        </div>
        <div class="container">
                <div class="row mt-4">
                        @foreach($baiviet3 as $value)
                        <div class="col">
                            <div class="card mb-3" style="width: 340px;height: 175px;">
                                <div class="row no-gutters">
                                    <div class="col-md-4">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="width: 175px;height: 175px;"alt="..."></a>
                                    </div>
                                    <div class="col-md-8">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                            <div class="card-body">
                                                <p class="card-title ml-5" style="font-size: 13pt">{{Str::limit($value['TieuDe'],100)}}</p>
                                            </div>
                                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
                <div class="row mt-4">
                        @foreach($baiviet4 as $value)
                        <div class="col">
                                <div class="card" style="width: 340px;">
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img-top" style="width: 338px; height: 248px;" alt="..."></a>
                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                            <div class="card-body">
                                                <h5 class="card-title">{{Str::limit($value['TieuDe'],120)}}</h5>
                                            </div>
                                        </a>
                                </div>
                        </div>
                        @endforeach
                </div>
        </div>

        {{-- MULTIMEDIA --}}
        <div class="container-fluid darkslategray mt-4">
               <div class="container">
                    <br>

                    <h1 style="color: white;">Top lượt xem</h1>
                    <div class="row">
                            <div class="col-sm-8">
                                    @foreach($baiviet_top1 as $value)
                                    <div class="card bg-dark" style="width: 720px;">
                                            <img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="width:720px;max-height: 400px"alt="...">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}" id="white">
                                            <div class="card-img-overlay">
                                            <h5 class="card-title" style="font-size: 24pt;  text-shadow: 3px 3px #2F4F4F;">{{$value['TieuDe']}}</h5>
                                                <p class="card-text mt-5" style="font-size: 16pt; padding-top: 100px;  text-shadow: 2px 2px #2F4F4F; text-align: justify;">{{$value['TomTat']}}</p>
                                            </div>
                                            </a>
                                    </div>
                                    @endforeach
                            </div>
                            <div class="col-sm-4">
                                    @foreach($baiviet_top2 as $value)
                                    <div class="card bg-dark" style="width: 340px;">
                                            <img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="width: 340px;max-height: 400px"alt="...">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}" id="white">
                                            <div class="card-img-overlay">
                                              <h5 class="card-title" style="padding-top: 5px;  text-shadow: 3px 3px #2F4F4F;">{{$value['TieuDe']}}</h5>
                                              <p class="card-text " style="padding-top: 170px;  text-shadow: 2px 2px #2F4F4F; text-align: justify;">{{$value['TomTat']}}</p>
                                            </div>
                                            </a>
                                    </div>
                                    @endforeach
                            </div>
                        </div>
                        <div class="row mt-4">
                            @foreach($baiviet_top3 as $value)
                            <div class="col">
                                    <div class="card bg-dark" style="width: 340px;">
                                            <img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="width: 340px;height: 200px"alt="...">
                                            <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}" id="white">
                                            <div class="card-img-overlay">
                                                <h5 class="card-title" style="text-shadow: 1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">{{$value['TieuDe']}}</h5>
                                            </div>
                                            </a>
                                    </div>
                            </div>
                            @endforeach
                        </div>
               </div>
        </div>
        <div class="container mt-4">
                <div class="row ml-1">
                        <div class="col-sm-8">
                            @foreach ($baiviet5 as $value)
                            <div class="row">
                                    <div class="card mb-3 mt-4" style="max-width: 720px  ;">
                                            <div class="row no-gutters">
                                              <div class="col-md-4">
                                                    <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                                        <img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="height: 200px;width: 200px;"alt="...">
                                                    </a>
                                              </div>
                                              <div class="col-md-8">
                                                <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                                <div class="card-body">
                                                  <h5 class="card-title">{{Str::limit($value['TieuDe'],150)}}</h5>
                                                  <p class="card-text">{{Str::limit($value['TomTat'],220)}}</p>
                                                  <p class="card-text card-bottom"><small class="text-muted">Đăng vào lúc: {{$value['created_at']}}</small></p>
                                                </div>
                                                </a>
                                              </div>
                                            </div>
                                    </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-sm-4">
                            <p class="mt-4" style="font-style: italic">Chứng khoán</p>
                            <!-- TradingView Widget BEGIN -->
                                <div class="row ml-0">
                                        <div class="tradingview-widget-container">
                                                <div id="tradingview_86707"></div>
                                                <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank"><span class="blue-text">AAPL Chart</span></a> by TradingView</div>
                                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                                <script type="text/javascript">
                                                new TradingView.widget(
                                                {
                                                "width": 340,
                                                "height": 410,
                                                "symbol": "NASDAQ:AAPL",
                                                "interval": "D",
                                                "timezone": "Etc/UTC",
                                                "theme": "Light",
                                                "style": "1",
                                                "locale": "en",
                                                "toolbar_bg": "#f1f3f6",
                                                "enable_publishing": false,
                                                "allow_symbol_change": true,
                                                "container_id": "tradingview_86707"
                                                }
                                                );
                                                </script>
                                        </div>
                                </div>
                                <br>
                        </div>
                        <div class="ml-3">
                                @foreach ($baiviet6 as $value)
                                <div class="row">
                                        <div class="card mb-3 mt-4" style="width: 1105px;">
                                                <div class="row no-gutters">
                                                    <div class="col-2">
                                                        <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}"><img src="/storage/cover_images/{{$value['HinhAnh_Bia']}}" class="card-img" style="height: 200px;width: 200px;"alt="..."></a>

                                                    </div>
                                                  <div class="col-8" style="padding-left:55px;">
                                                    <a href="/baiviet/tieude/{{$value['TieuDeKhongDau']}}">
                                                        <div class="card-body">
                                                        <h5 class="card-title">{{Str::limit($value['TieuDe'],200)}}</h5>
                                                        <p class="card-text">{{Str::limit($value['TomTat'],400)}}</p>
                                                        <p class="card-text card-bottom"><small class="text-muted">Đăng vào lúc: {{$value['created_at']}}</small></p>
                                                        </div>
                                                    </a>
                                                  </div>
                                                </div>
                                        </div>
                                </div>
                                @endforeach
                        </div>
                </div>
        </div>
        <div class="container mt-4" id="app">
            <baiviet-component :lastid="{{json_encode($last_id)}}"></baiviet-component>
        </div>



            <script src="{{ asset('js/app.js') }}"></script>
            <script>
                setTimeout(function()
                {
                var max = 200;
                var tot, str;
                $('.text').each(function() {
                    str = String($(this).html());
                    tot = str.length;
                    str = (tot <= max)
                        ? str
                    : str.substring(0,(max + 1))+"...";
                    $(this).html(str);
                });
                },500); // Delayed for example only.
            </script>

@endsection
