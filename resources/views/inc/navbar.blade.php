
    @php
        use App\ChuDe;
        $chude = ChuDe::all();
    @endphp
<div class="container">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark mb-5 border-bottom border-sencondary" style="background-color: darkslategrey;">
    <a class="navbar-brand" href="{{url('')}}"><h3 class="mt-1" style="padding-left: 100px;">NEWS 365</h3></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                @foreach ($chude as $item)
                    <li class="nav-item active">
                        <a class="nav-link nav-myfont ml-2 a_hover" href="/chude/{{utf8tourl(utf8convert($item['TenChuDe']))}}">{{$item['TenChuDe']}}</a>
                    </li>
                @endforeach

                @if(Auth::guest())
                <li class="nav-item">
                    <a class="nav-link mt-1 a_hover" href="{{ route('login') }}"><i class="fas fa-user"></i></a>
                </li>
                @else
                <li class="nav-item active">
                        <div class="dropdown dropleft">
                                <a class="nav-link active mt-1 dropdown a_hover" href="/" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-user"></i>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                @if(Auth::check() == true && Auth::user()->quyenhan == "quanly")
                                  <a class="dropdown-item" href="/quanly" style="font-weight: bold;"><i class="fas fa-cog fa-spin"></i> Quản lý</a>
                                  <a class="dropdown-item" href="/quanly/baiviet/them" style="font-weight: bold;"><i class="fas fa-plus ml-0"></i> Thêm bài viết</a>
                                @endif
                                    <a class="dropdown-item" href="/baiviet/danhsach" style="font-weight: bold;"><i class="fas fa-search"></i><span class="ml-1">Tìm kiếm bài viết</span></a>
                                    <a class="dropdown-item" href="/nguoidung/thongtin" style="font-weight: bold;"><i class="fas fa-info ml-1"></i>  <span class="ml-2">Thông tin cá nhân</span></a>
                                    <a class="dropdown-item" href="/nguoidung/doimatkhau" style="font-weight: bold;"><i class="fas fa-key"></i> <span class="ml-1">Đổi mật khẩu</span></a>
                                    <a class="dropdown-item" href="{{route('logout')}}" style="font-weight: bold;" onclick='return confirm("Bạn có muốn đăng xuất?")'><i class="fas fa-sign-out-alt"></i> <span class="ml-1">Logout</span></a>
                                </div>
                        </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link mt-1 a_hover" href="{{url('logout')}}" onclick='return confirm("Bạn có muốn đăng xuất?")'><i class="fas fa-sign-out-alt"></i></a>
                </li>
                @endif
             </ul>
        </div>
    </nav>
    <div style="padding-top: 75px;"></div>
</div>
<script>
    // Don't forget to add CSS for your custom styles.

$(document).ready(function(){
$(window).scroll(function () {
if ($(this).scrollTop() > 50) {
    $('#back-to-top').fadeIn();
} else {
    $('#back-to-top').fadeOut();
}
});
// scroll body to 0px on click
$('#back-to-top').click(function () {
$('body,html').animate({
    scrollTop: 0
}, 400);
return false;
});
});

</script>
