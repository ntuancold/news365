@extends('layouts.admin')

@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header" style="background-color: #2F4F4F;"><a href="{{ url('') }}" style="color:white; ">Trang Chủ</a></div>
                    <div class="card-body">

                        <table class="table table-dark table-hover"  style="background-color: #2F4F4F;">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center" scope="col" width="10%">#</th>
                                    <th class="text-center" scope="col" width="20%">Tên người dùng</th>
                                    <th class="text-center" scope="col" width="20%">Email</th>
                                    <th class="text-center" scope="col" width="10%">Quyền hạn</th>
                                    <th class="text-center" scope="col" width="20%">Khóa</th>
                                    <th class="text-center" scope="col" width="10%">Sửa</th>
                                    <th class="text-center" scope="col" width="10%">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count = 1; @endphp
                                @foreach($nguoidung as $value)
                                    <tr>
                                        <td class="text-center">{{ $count++ }}</td>
                                        <td class="text-center">{{ $value->name }}</td>
                                        <td class="text-center">{{ $value->email }}</td>
                                        <td class="text-center">
                                            @if($value->quyenhan == "quanly")
                                                <h5><span class="badge badge-warning"><i class="fas fa-user-shield"></i> Admin</span></h5>
                                            @else
                                                <span class="badge badge-secondary"><i class="fas fa-user"></i> Người dùng</span>
                                            @endif
                                        </td>
                                        @if($value['khoa'] == 1)
                                        <td class="text-center">
                                                <a href="/quanly/nguoidung/kichhoat/{{$value->id}}/khoa" class="btn btn-primary" onclick='return confirm("Bạn có chắc muốn khóa tài khoản này?")'><i class="far fa-smile-wink"></i>Kích hoạt</a>
                                        </td>
                                        @else
                                        <td class="text-center">
                                                <a href="/quanly/nguoidung/kichhoat/{{$value->id}}/kichhoat" class="btn btn-warning" onclick='return confirm("Bạn có chắc muốn mở khóa tài khoản này?")'><i class="far fa-frown"></i>Bị khóa</a>
                                        </td>
                                        @endif
                                        <td class="text-center"><a href="/quanly/nguoidung/danhsach/sua/{{$value->id}}" class="btn btn-info"><i class="fas fa-tools"></i> Sửa</a></td>
                                        <td class="text-center"><a href="/quanly/nguoidung/xoa/{{$value->id}}" class="btn btn-danger" onclick='return confirm("Bạn có chắc muốn xóa tài khoản này?")'><i class="fas fa-trash"></i> Xóa</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



