@extends('layouts.admin')

@section('title','Sửa người dùng')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: #2F4F4F; color:white;">{{ __('Sửa thông tin người dùng') }}</div>

                <div class="card-body">
                    <form method="POST" action="/quanly/nguoidung/danhsach/sua">
                        @csrf
                        <input type="hidden" name="ID" value="{{$nguoidung['id']}}" >
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right" >Tên Người Dùng</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$nguoidung['name']}}">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Địa chỉ E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$nguoidung['email']}}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="quyenhan" class="col-md-4 col-form-label text-md-right">{{ __('Quyền hạn') }}</label>

                                <div class="col-md-6">
                                    <select id="quyenhan" type="text" class="form-control @error('email') is-invalid @enderror" name="quyenhan" required autocomplete="email">
                                        <option value="quanly">Quản lý</option>
                                        <option value="nguoidung">Ngươi dùng</option>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </select>
                                </div>
                        </div>

                        <div class="form-group row">
                                <label for="kichhoat" class="col-md-4 col-form-label text-md-right">{{ __('Kích hoạt') }}</label>

                                <div class="col-md-6">
                                    <select id="quyenhan" type="text" class="form-control @error('email') is-invalid @enderror" name="khoa" required autocomplete="email">
                                        <option value="1">Kích hoạt</option>
                                        <option value="0">Khóa tài khoản</option>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </select>
                                </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary" onclick='return confirm("Bạn có muôn sửa người dùng {{$nguoidung["name"]}}")'>
                                    {{ __('Sửa người dùng') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
