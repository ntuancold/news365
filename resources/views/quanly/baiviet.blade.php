   <h2 class="mt-4" style="font-weight: bold;"><i class="fas fa-cog fa-spin"></i>Menu Quản Lý</h2>
        <div class="row mt-4">
                <div class="col">
                        <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm" aria-label="Search" style="width: 400px;">
                                <button class="btn btn-primary my-2 my-sm-0" type="submit"><i class="fas fa-search"></i>Tìm kiếm</button>
                        </form>
                </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                    <div class="row">
                            <div class="d-flex mr-2"><span class="btn">Tùy chỉnh</span></div>
                            <div class="d-flex">
                                    <div class="dropdown mr-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
                                        Offset
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-secondary">Reference</button>
                                        <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                        <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                        </div>
                                    </div>
                            </div>
                    </div>
                    <div class="row mt-4">
                        <a href="" class="btn btn-outline-dark ml-3" style="width: 400px; font-size: 16pt;">Quản lý chủ đề</a>
                    </div>
                    <div class="row mt-4">
                        <a href="" class="btn btn-outline-dark ml-3" style="width: 400px; font-size: 16pt;">Quản lý hình ảnh</a>
                    </div>
                    <div class="row mt-4">
                            <a href="" class="btn btn-outline-dark ml-3" style="width: 400px; font-size: 16pt;">Quản lý bình luận</a>
                    </div>
                    <div class="row mt-4">
                            <a href="" class="btn btn-outline-dark ml-3" style="width: 400px; font-size: 16pt;">Quản lý bài viết</a>
                    </div>
            </div>
        </div>
