@extends('layouts.admin')

@section('title','Quản lý WEBSITE 365')

@section('content')
    <div class="container">
        <div class="text-center mt-3"><h1>Các danh mục quản lý</h1></div>
            <div class="row">
                <div class="col">
                        <p class="mt-1" style="font-weight: bold;">Bài viết</p>
                        <div class="row ml-0">

                                <a class="btn btn-primary" href="/baiviet/danhsach"><i class="fa fa-list" aria-hidden="true"></i> Quản lý danh sách bài viết</a>
                                <a class="btn btn-primary ml-2" href="/quanly/baiviet/them"><i class="fa fa-plus" aria-hidden="true"></i> Thêm bài viết</a>
                        </div>
                        <p class="mt-3" style="font-weight: bold;">Chủ đề</p>
                        <div class="row ml-0">

                                <a class="btn btn-warning" href="/quanly/chude"><i class="fas fa-atlas"></i> Quản lý danh sách chủ đề</a>
                                <a class="btn btn-warning ml-2" href="/quanly/chude/them"><i class="fa fa-plus" aria-hidden="true"></i> Thêm chủ đề</a>
                        </div>
                </div>
                <div class="col">
                        <p class="mt-1" style="font-weight: bold;">Người dùng</p>
                        <div class="row ml-0">
                                <a class="btn btn-success" href="quanly/nguoidung/danhsach"><i class="fa fa-user" aria-hidden="true"></i> Quản lý danh sách người dùng</a>
                        </div>
                        <p class="mt-3" style="font-weight: bold;">Bình luận</p>
                        <div class="row ml-0">
                                <a class="btn btn-warning" href="/quanly/binhluan/danhsach"><i class="fa fa-list" aria-hidden="true"></i> Quản lý danh sách bình luận</a>
                        </div>
                </div>
            </div>

    </div>
@endsection
