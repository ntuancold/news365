<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Bài viết


Route::get('/loadmorebaiviet','BaiVietAPI@load_data');
Route::get('/danhsachbaiviet','BaiVietAPI@danhsach');
Route::post('/search','BaiVietAPI@search');

Route::get('/chude_loadmore','ChuDeController@ChuDeBaiVietLoadMore');

Route::get('/binhluan','BinhLuanController@Index');
Route::post('/binhluan','BinhLuanController@BinhLuanADD');

Route::post('/binhluan/xoa','BinhLuanController@BinhLuanDelete');
Route::post('/binhluan/update','BinhLuanController@BinhLuanUpdate');
Route::post('/binhluan/bokichhoat','BinhLuanController@BinhLuanBokichhoat');
