<?php

use App\BinhLuan;
use App\User;
use App\Facade\MyURL;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use MyNamespace\URL;

Route::get('','HomeController@index');

Route::get('/delete',function(){
    $binhluan = BinhLuan::orderBy('created_at','desc');
    $binhluan->delete();
});
Route::get('/dashboard', 'DashboardController@index');


//Bài viết
Route::get('/testapi',function(){
    $id =  User::where('remember_token','o2iS3nMosHhQ21LbvCGqIkAPUtMbar0c2bMuXgX6w04GD790PqROCGC7PMtX')->first()->id;

    return $id;
});

Route::get('/hack','BinhLuanController@hack');



//public route
Route::get('/chude/{tenchude}/','ChuDeController@getTenChuDe');

Route::get('/baiviet/tieude/{tieudebaiviet}','BaiVietController@getNoiDungBaiViet');
Route::get('chude/baiviet/tieude/{tieudebaiviet}','BaiVietController@getNoiDungBaiViet');

Route::post('/baiviet/tieude/{tieudebaiviet}','BaiVietController@postBinhLuanBaiViet');

Route::get('/baiviet/danhsach','BaiVietController@getDanhSachBaiViet');
Route::post('/baiviet/danhsach','BaiVietController@postDanhSachBaiVietCanTim');

Route::get('/baiviet/{tenchude}/danhsach','BaiVietController@getDanhSachBaiViet_TheoChuDe');
Route::get('/baiviet/{tenchude}/danhsach','BaiVietController@getDanhSachBaiViet_TheoChuDe');

Route::get('/nguoidung/doimatkhau','NguoiDungController@getDoiMatKhau');
Route::post('/nguoidung/doimatkhau','NguoiDungController@postDoiMatKhau');

Route::get('/binhluan/xoa/{id}','BaiVietController@getXoaBinhLuan');
Route::post('/binhluan/sua','BaiVietController@postSuaBinhLuan');
//private route
//edit
Route::group(['prefix' => 'quanly', 'middleware' => ['auth','quanly']],function(){
    Route::get('','HomeController@getQuanly');
    Route::get('/chude','ChuDeController@getIndex');
    Route::get('/chude/them','ChuDeController@getThemChuDe');
    Route::post('/chude/them','ChuDeController@postThemChuDe');
    Route::get('/chude/{id}/sua','ChuDeController@getSuaChuDe' );
    Route::post('/chude/sua','ChuDeController@postSuaChuDe' );
    Route::get('/chude/{id}/xoa','ChuDeController@getXoaChuDe');
    Route::get('/baiviet/them','BaiVietController@getThemBaiViet');
    Route::post('/baiviet/them','BaiVietController@postThemBaiViet');

    Route::get('/baiviet/sua/{id}','BaiVietController@getSuaBaiViet');
    Route::post('/baiviet/sua','BaiVietController@postSuaBaiViet' );

    Route::get('/baiviet/xoa/{id}','BaiVietController@getXoaBaiViet');


    Route::get('/nguoidung/danhsach','NguoiDungController@getDanhSachNguoiDung');

    Route::get('/nguoidung/danhsach/sua/{manhanvien}','NguoiDungController@getSuaNguoiDung');
    Route::post('/nguoidung/danhsach/sua','NguoiDungController@postSuaNguoiDung');

    Route::get('/nguoidung/kichhoat/{id}/kichhoat','NguoiDungController@getKichHoatNguoiDung');
    Route::get('/nguoidung/kichhoat/{id}/khoa','NguoiDungController@getKhoaNguoiDung');

    Route::get('/nguoidung/xoa/{id}','NguoiDungController@getXoaNguoiDung');

    Route::get('/binhluan/danhsach','BaiVietController@getDanhSachBinhLuan');

    Route::get('/binhluan/kichhoat/{id}','BaiVietController@getKichHoatBinhLuan');
    Route::get('/binhluan/bokichhoat/{id}','BaiVietController@getBoKichHoatBinhLuan');
    // Route::get('/nguoidung/khoataikhoan/{manhanvien}',function($manhanvien){
    //     echo $manhanvien;
    // });
});

    Route::get('/facade', function(){
        return MyURL::test();
});

Auth::routes();

Route::get('logout','HomeController@getLogout');
